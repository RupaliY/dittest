const gulp = require('gulp');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');

gulp.task('js', function(){
  gulp.src('src/js/*.js')
      //.pipe(uglify())
      .pipe(gulp.dest('dist/js'));
});

gulp.task('jslib', function(){
  gulp.src('src/lib/*.js')
      .pipe(uglify())
      .pipe(gulp.dest('dist/lib'));
});

gulp.task('scss', () => {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dist/css/'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('default', gulp.series(['scss','js','jslib']));

gulp.watch('src/scss/**/*.scss', gulp.series('scss'));
gulp.watch('src/js/*.js', gulp.series('js'));

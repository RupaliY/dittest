
window.addEventListener('load', (event) => {
    loadJson();
});

function loadJson() {
    var jsonPath = '../src/data.json'
    ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', jsonPath);

    ourRequest.onload = function(){
        var productJson = JSON.parse(ourRequest.responseText);
        bindProductData(productJson.carouselData);
    };
    ourRequest.send();
}

function bindProductData(_carouselData){

    new Vue({
      el: '#mycarousel',
      data: {
        productData: _carouselData
      }
    })

    createCarousel();
}

function createCarousel() {

    $('.slick-slider').slick({
      autoplay: true,
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    });

}
